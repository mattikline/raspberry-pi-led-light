#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int main()
{
	int args[3];
	
	do {
		printf("Please enter how many times you want the LED to blink: ");
		scanf("%d", &args[0]);
	} while (args[0] < 1 || args[0] >= 10);
	
	do {
		printf("Please enter how long you want the LED to stay on during a blink (in milliseconds, 1000 = 1 second): ");
		scanf("%d", &args[1]);
	} while (args[1] < 1 || args[1] >= 10000);
	
	do {
		printf("Please enter how long you want the LED to stay off between blinks: ");
		scanf("%d", &args[2]);
	} while (args[2] < 1 | args[2] >= 10000);
	
	char numBlinks[2];
	sprintf(numBlinks, "%d", args[0]);
	
	char blinkOnTime[4];
	sprintf(blinkOnTime, "%d", args[1]);
	
	char blinkOffTime[4];
	sprintf(blinkOffTime, "%d", args[2]);
	
	char *argv[] = (numBlinks, blinkOnTime, blinkOffTime, NULL);
	
	pid_t pid = fork();
	
	if (pid == 0)
		execvp("sudo ./led", argv);
	else
		wait();
	
	return 0;
}
