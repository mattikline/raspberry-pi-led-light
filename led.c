#include <stdio.h> // Used with printf()
#include <sys/mman.h> // Library for memory management, used with mmap()
#include <fcntl.h> // Used with open()
#include <unistd.h> // Used with usleep()
#include <stdlib.h> // Used with atoi()

// According to the BCM2837 ARM peripherals documentation which applies to
// the raspberry pi 3 (https://cs140e.sergio.bz/docs/BCM2837-ARM-Peripherals.pdf),
// physical addresses for peripherals range from 0x3F000000 to 0x3FFFFFFF,
// thus 0x3F000000 is the base peripheral address and we will define it as such.
#define BCM2837_PERI_BASE 0x3F000000

// Also according to page 6 of the same document, the bus addresses for peripherals
// are set up to map onto the peripheral bus address range starting at 0x7E000000.
// According to page 90, the address of the first GPIO pin is 0x7E200000.
// Thus when we add 0x200000 to the peripheral base address
// we can define the GPIO base address.
#define GPIO_BASE (BCM2837_PERI_BASE + 0x200000)

// This variable will be used to store the physical memory from /dev/mem.
// fd stands for file descriptor, since mem_fd will be passed as the fd parameter to mmap.
int mem_fd;

// Pointer to the mapped memory.
void *gpio_map;

// Volatile variable for sending values to the gpio pins.
volatile unsigned int *gpio;

// Macro that sets pin g as the input.
// gpio + ((g)) / 10 is the register address that contains the GPFSEL bits of pin g.
// There are three GPFSEL bits per pin, 000 for input and 001 for output,
// and the location is given by (((g)) % 10) * 3.
// An AND operation is used between the GPFSEL register
// and a string of made up of 1's and the input bits 000.
// The string is bitshifted by 7 (111 in binary) by (((g) % 10) * 3 places and taking the inverse.
#define INP_GPIO(g) *(gpio + ((g) / 10)) &= ~(7 << (((g) % 10) * 3))

// Macro that sets pin g as the output.
// gpio + ((g)) / 10 is the same as before.
// The string is instead bitshifted by 1 and an OR operation is used on the string without inverting it.
// INP_GPIO(g) should always be used before OUT_GPIO(g) so that it's certain the other two bits are zero.
#define OUT_GPIO(g) *(gpio + ((g) / 10)) |= (1 << (((g) % 10) * 3))

// The GPSET register is located 7 32-bit registers further
// than the base gpio register according to page 90 of the documentation.
#define GPIO_SET *(gpio + 7)

// The GPCLR register is located 10 32-bit registers further
// than the base gpio register accorind to page 90 of the documentation.
#define GPIO_CLR *(gpio + 10)

int main(int argc, char *argv[])
{
	// First program parameter is how many times the LED will blink, default 1
	int numBlinks;
	if (argv[1] == NULL || atoi(argv[1]) < 1)
		numBlinks = 1;
	else
		numBlinks = atoi(argv[1]);

	// Second program parameter is how long in milliseconds LED will stay on, user enters in milliseconds (1000 milliseconds = 1 second), default 0.5 second (500000 microseconds)
	int blinkOnTime;
	if (argv[2] == NULL || atoi(argv[2]) < 1)
		blinkOnTime = 500;
	else
		blinkOnTime = atoi(argv[2]); // User enters intended milliseconds, usleep() uses microseconds

	// Third program parameter is how long in milliseconds LED will stay off, default = blinkOn
	int blinkOffTime;
	if (argv[3] == NULL || atoi(argv[3]) < 1)
		blinkOffTime = blinkOnTime;
	else
		blinkOffTime = atoi(argv[3]); // User enters intended milliseconds, usleep() uses microseconds
	
	printf("Blinking %d times, LED on for %d milliseconds and off for %d milliseconds\n", numBlinks, blinkOnTime, blinkOffTime);

	// Will set mem_fd as the file descriptor for the physical memory from /dev/mem
	// and check if it failed or not. This requires the program to be run as root.
	if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC)) < 0)
	{
		printf("Could not open /dev/mem, try running as root.\n");
		return -1;
	}
	
	// gpio_map will be given the value of the pointer to the mapped area.
	gpio_map = mmap(
	NULL,					// The address parameter is null so the kernel chooses by itself
							// the (page-aligned) address at which to create the mapping.
	4096,					// The length of the mapping.
	PROT_READ|PROT_WRITE,	// Parameter for memory protection of the mapping,
							// in this case pages may be read or written.
	MAP_SHARED,				// This shares the mapping, so updates to the mapping
							// are visible to other processes mapping the same region.
	mem_fd,					// Uses the file description to map to /dev/mem.
	GPIO_BASE);				// The offset, or where the contents of the file mapping start at.
	
	// After the mmap() call has returned, the file descriptor, fd,
	// can be closed immediately without invalidating the mapping.
	close(mem_fd);
	
	// Simply checks if the mmap mapping failed.
	if (gpio_map == MAP_FAILED)
	{
		printf("mmap failed.\n");
		return -1;
	}
	
	// Uses volatile memory as the value can change at any time without the program knowing.
	gpio = (volatile unsigned *)gpio_map;

	// Value that will be used for which pin to use
	int g = 17;

	// Sets value g as the input and then the output
	INP_GPIO(g);
	OUT_GPIO(g);

	// Each loop blinks LED
	for (int i = 0; i < numBlinks; i++)
	{
		// Toggles pin g
		GPIO_SET = 1 << g;

		// Program halts and LED stays on for blinkOnTime number of microseconds times 1000 for milliseconds
		usleep(blinkOnTime * 1000);

		// Clears pin g, turns it off
		GPIO_CLR = 1 << g;

		usleep(blinkOffTime * 1000);
	}
	
	return 0;
}
