# Raspberry Pi 3 LED Light Device Driver

This is a device driver for a LED light connected to a Raspberry Pi 3 through the GPIO pins.

### Compiling

To compile, type 
```
gcc led.c -o led
```

### Usage

To use, you must wire an LED to GPIO pin 17.

To run, type
```
sudo ./led
```

The program can take three command line arguments

1. Number of times LED will blink
2. Time in milliseconds LED will stay on during a blink
3. Time in milliseconds LED will stay off during a blink

If the third argument is empty, it will be set equal to the second. If the second argument is empty, they will both default to 500 milliseconds. If the first argument is empty, it will blink only once.

Using arguments may look like
```
sudo ./led 10 100 250
```